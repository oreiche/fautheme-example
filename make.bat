set TEXINPUTS=.\themefau

pdflatex.exe -shell-escape presentation.tex
biber.exe presentation
pdflatex.exe -shell-escape presentation.tex

del *.aux *.bbl *.bcf *.blg *.nav *.out *.snm *.log *.toc *.vrb
pause
